<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Model;
use App\Models\RequestPersonal;
use App\Models\RequestProperty;
use App\Services\Cache;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class RequestsController extends Controller
{
    public function startAction()
    {
        Cache::set(sprintf('request_%s', Auth::id()), [[]], 60*60*24);
        return redirect(route('user.request.property', ['id' => 'new']));
    }

    public function continueAction()
    {
        return redirect(route('user.request.property', ['id' => 'new']));
    }

    public function propertyAction(Request $request, $id = null)
    {
        $model = $this->processModel($id, RequestProperty::class, $request);
        if ($request->isMethod('post') && !$model->hasErrors()) {
            return redirect(route('user.request.personal', ['id' => $id]));
        }
        return view('user.requests.property', [
            'model' => $model
        ]);
    }

    public function personalAction(Request $request, $id = null)
    {
        $model = $this->processModel($id, RequestPersonal::class, $request);
        return view('user.requests.personal', [
            'model' => $model
        ]);
    }

    private function processModel($id, $class, Request $request)
    {
        if ($id == 'new' && $models = Cache::get(sprintf('request_%s', Auth::id()))) {
            /** @var Model $model */
            $model = new $class;
            if (!empty($models[$class])) {
                $model->setAttributes($models[$class]);
            }
        } elseif ($model = $class::query()->where('requestId', $id)->first()) {

        } else {
            throw new NotFoundHttpException();
        }
        if ($request->isMethod('post')) {
            $model->setAttributes($request->input());
            $model->buildingLease = $request->has('buildingLease');
            $model->rightOfUse = $request->has('rightOfUse');
            $model->communityOfHeirs = $request->has('communityOfHeirs');
            $model->validate();
            if (!$model->id) {
                $models = Cache::get(sprintf('request_%s', Auth::id()));
                $models[$class] = $model->toArray();
                Cache::set(sprintf('request_%s', Auth::id()), $models, 60*60*24);
            }
        }
        return $model;
    }

}