<?php

namespace App\Http\Controllers\User;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function settingsAction(Request $request)
    {
        /** @var User $model */
        $model = Auth::user();
        $infoErrors = [];
        $passErrors = [];
        if ($request->has('info')) {
            $model->setAttributes($request->input('info'));
            if ($model->save()) {
                $request->session()->flash('message', 'Changes saved.');
            } else {
                $infoErrors = $model->getErrors();
            }
        }
        if ($request->has('pass')) {
            Validator::extend('old_password', function ($attribute, $value, $parameters, \Illuminate\Validation\Validator $validator) use ($model) {
                $validator->customMessages['old_password'] = 'The :attribute does not match to current password.';
                return Auth::validate(['email' => $model->email, 'password' => $value]);
            });
            /** @var \Illuminate\Validation\Validator $validator */
            $validator = Validator::make($request->input('pass'), [
                'old_password' => 'required|old_password',
                'password' => 'required|min:6|max:255|confirmed'
            ]);
            if (!$validator->fails()) {
                $model->password = bcrypt($request->input('pass')['password']);
                $model->unsafeSave();
                $request->session()->flash('message', 'Password  changed.');
            } else {
                $passErrors = $validator->errors();
            }
        }

        return view('user.settings', [
            'model' => $model,
            'infoErrors' => $infoErrors,
            'passErrors' => $passErrors
        ]);
    }
}