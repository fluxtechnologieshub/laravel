<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class Model extends EloquentModel
{
    /**
     * @var \Illuminate\Validation\Validator
     */
    protected $_validator;

    protected $_scenario;

    /**
     * Validation Scenarios
     */
    public function scenarios()
    {

    }

    /**
     * Validation Rules
     */
    public function rules()
    {

    }

    public function setAttributes($values)
    {
        foreach (self::getFields() as $field) {
            if (array_key_exists($field, $values)) {
                $this->setAttribute($field, $values[$field]);
            }
        }
    }

    /**
     * Set scenario
     *
     * @param $scenario
     */
    public function setScenario($scenario)
    {
        $this->_scenario = $scenario;
    }

    /**
     * Validates models
     *
     * @param null $data
     * @param null $scenario
     * @return array|\Illuminate\Support\MessageBag
     */
    public function validate($data = null, $scenario = null)
    {
        $rules = $this->rules();
        if (!$scenario && $this->_scenario) {
            $scenario = $this->_scenario;
        }
        if ($scenario) {
            foreach ($rules as $field => $rule) {
                if (!in_array($field, $this->scenarios()[$scenario])) {
                    unset($rules[$field]);
                }
            }
        }
        $this->_validator = Validator::make($data ? $data : $this->toArray(), $rules);
        $this->_validator->setAttributeNames(static::getFieldLabels()->toArray());

        if (!$this->_validator->fails()) {
            return [];
        }
        return $this->_validator->errors();
    }

    /**
     * Check if model has errors
     *
     * @return bool
     */
    public function hasErrors()
    {
        return $this->_validator ? $this->_validator->fails() : false;
    }

    public function hasError($field)
    {
        return $this->hasErrors() ? $this->_validator->errors()->has($field) : false;
    }

    public function getError($field)
    {
        return $this->hasError($field) ? $this->_validator->errors()->get($field)[0] : '';
    }

    /**
     * Returns ErrorMessageBag
     *
     * @return \Illuminate\Support\MessageBag
     */
    public function getErrors()
    {
        return $this->_validator->errors();
    }

    /**
     * @inheritdoc
     */
    public function save(array $options = [])
    {
        $this->validate();
        if ($this->hasErrors()) {
            return false;
        }
        return parent::save($options);
    }

    /**
     * Save model without validation
     *
     * @param array $options
     * @return bool
     */
    public function unsafeSave(array $options = [])
    {
        return parent::save($options);
    }

    /**
     * @return \Illuminate\Support\Collection|array
     */
    public static function getFieldLabels()
    {
        return collect(array_flip(self::getFields()))->map(function($label, $field) {
            return ucwords(preg_replace(array('/(?<=[^A-Z])([A-Z])/', '/(?<=[^0-9])([0-9])/'), ' $0', Str::camel($field)));
        });
    }

    /**
     * Returns fields
     *
     * @param array $without
     * @return array
     */
    public static function getFields(array $without = [])
    {
        return array_values(array_diff(Schema::getColumnListing((new static())->getTable()), $without));
    }
}