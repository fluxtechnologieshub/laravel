<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class Zone
 * @package App\Models
 *
 * @property int $id
 * @property int $providerId
 * @property string $name
 * @property int $mortgageOffers
 * @property int $ammortisationOffers
 * @property string $maximumAcceptabilityOfRisk
 * @property string $maximumLoan2ValueRatio
 * @property string $maximumForwardPeriod
 * @property string $calculatoryInterestRate
 * @property string $created_at
 * @property string $update
 * @property ZoneRange[]|\Illuminate\Support\Collection $ranges
 * @property ZoneType[]|\Illuminate\Support\Collection $types
 */
class Zone extends Model
{
    protected $table = 'provider_zones';
    public $timestamps = false;

    public function rules()
    {
        return [
            'providerId' => 'max:255|required',
            'name' => 'max:255|required',
            'mortgageOffers' => 'boolean|required',
            'ammortisationOffers' => 'boolean|required',
            'maximumAcceptabilityOfRisk' => 'integer|required',
            'maximumLoan2ValueRatio' => 'integer|required',
            'maximumForwardPeriod' => 'max:255|required',
            'calculatoryInterestRate' => 'integer|required',
        ];
    }

    public function ranges()
    {
        return $this->hasMany('App\Models\ZoneRange', 'zoneId', 'id');
    }

    public function types()
    {
        return $this->hasMany('App\Models\ZoneType', 'zoneId', 'id');
    }

    public function getSelectedTypes()
    {
        return $this->types->groupBy('type')->map(function(Collection $item) {
            return $item->map(function(ZoneType $item) {
                return $item->typeId;
            });
        })->toArray();
    }

    public static function getFieldLabels()
    {
        $labels =  parent::getFieldLabels();
        $labels['maximumLoan2ValueRatio'] = 'Maximum Loan To Value Ratio';
        return $labels;
    }
}